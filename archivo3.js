// Ejemplo de noticias (puedes reemplazarlo con datos reales)
const noticias = [
    {
        titulo: "Nueva expansión anunciada",
        descripcion: "¡Descubre los detalles emocionantes de la próxima expansión de World of Warcraft!",
        fecha: "2023-08-16"
    },
    {
        titulo: "Evento especial en Azeroth",
        descripcion: "Participa en el evento mundial y gana recompensas épicas.",
        fecha: "2023-08-18"
    }
    // Agrega más noticias aquí
];

// Función para mostrar las noticias en la página
function mostrarNoticias() {
    const newsContainer = document.getElementById("news-container");

    noticias.forEach(noticia => {
        const noticiaElement = document.createElement("div");
        noticiaElement.classList.add("noticia");

        const tituloElement = document.createElement("h3");
        tituloElement.textContent = noticia.titulo;

        const descripcionElement = document.createElement("p");
        descripcionElement.textContent = noticia.descripcion;

        const fechaElement = document.createElement("p");
        fechaElement.textContent = `Publicado el ${noticia.fecha}`;

        noticiaElement.appendChild(tituloElement);
        noticiaElement.appendChild(descripcionElement);
        noticiaElement.appendChild(fechaElement);

        newsContainer.appendChild(noticiaElement);
    });
}

// Llama a la función para mostrar las noticias al cargar la página
document.addEventListener("DOMContentLoaded", mostrarNoticias);