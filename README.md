
## APRENDIENDO GIT
Hola GIT!!!
## README DEL FEATURING-MENU
Fusionamos el contenido este a la rama main
## TEXTO DEL FEATURE-LOGIN

En la penumbra danzaban las sombras, un enredo de misterios susurrados por la brisa nocturna. Bajo la luna plateada, secretos olvidados se alzaban, mientras el mundo dormía ajeno a la sinfonía de lo oculto. Cada rincón, un relato por descubrir; cada suspiro, un eco de los anhelos más profundos. El tiempo parecía detenerse, atrapado entre destellos de estrellas y enredaderas de enigmas. En este rincón de lo invisible, la realidad y los sueños tejían su danza, y solo aquellos dispuestos a perderse podrían desentrañar el enigma de la noche.

![Logo_de_WoW](https://cdn.freebiesupply.com/logos/large/2x/world-of-warcraft-logo-png-transparent.png)

## "World of Warcraft" 
(WoW) es un popular videojuego en línea de rol multijugador masivo en línea (MMORPG) desarrollado y publicado por Blizzard Entertainment. El juego fue lanzado el 23 de noviembre de 2004 y ha experimentado múltiples expansiones y actualizaciones a lo largo de los años. A continuación, te proporcionaré una breve historia de "World of Warcraft" hasta mi última fecha de conocimiento en septiembre de 2021:

## Vanilla WoW (2004-2006): 
El juego se lanzó en su versión inicial, conocida como "Vanilla WoW". Los jugadores exploraron los continentes de Kalimdor y los Reinos del Este, eligiendo entre dos facciones principales: la Alianza y la Horda. La historia se centró en la lucha entre estas dos facciones, así como en la amenaza de la Legión Ardiente, una fuerza demoníaca.

## The Burning Crusade (2007): 
La primera expansión, "The Burning Crusade", llevó a los jugadores a Outland, el mundo destrozado de los orcos, gobernado por el traidor Illidan Stormrage. También se introdujo la raza de los elfos de sangre y los draenei, así como nuevas clases jugables.

## Wrath of the Lich King (2008): 
La expansión "Wrath of the Lich King" se centró en la lucha contra el Rey Exánime, Arthas Menethil, en el continente de Northrend. Se introdujo la clase Caballero de la Muerte y se exploraron historias relacionadas con la Plaga y la historia de Warcraft III.

## Cataclysm (2010): 
"Cataclysm" cambió drásticamente el mundo de Azeroth, con la aparición de un dragón corrupto llamado Alamuerte, que causó destrucción en todo el mundo. Se introdujeron dos razas jugables: goblins y huargen.

## Mists of Pandaria (2012): 
Los jugadores exploraron la tierra de Pandaria y se unieron a la Alianza y la Horda en una lucha por el control de la energía chi y el destino de los pandaren. Se introdujeron los pandaren como raza jugable y la clase Monje.

## Warlords of Draenor (2014): 
Los jugadores viajaron al pasado de Draenor para enfrentarse a los líderes orcos antes de su caída en la Legión Ardiente. La expansión introdujo construcción de fortalezas y la raza de los orcos Mag'har como aliados.

## Legion (2016): 
Los jugadores lucharon contra la Legión Ardiente y su líder, Sargeras, en múltiples mundos. Se agregaron las clases Cazador de demonios y Pícaro Subterfugio.

## Battle for Azeroth (2018): 
La expansión "Battle for Azeroth" enfrentó a la Alianza y la Horda en un conflicto a gran escala. Cada facción buscaba controlar el recurso conocido como Azerita. Se introdujeron las razas jugables de los elfos del Vacío y los tauren Monte Alto.

## Shadowlands (2020): 
Los jugadores ingresaron al reino de las Tierras Sombrías, el más allá de Azeroth, donde las almas se dirigen después de la muerte. La expansión se centró en los pactos y la lucha contra el Jailer, un nuevo villano.

![Logo_de_WoW](https://cdn.freebiesupply.com/logos/large/2x/world-of-warcraft-logo-png-transparent.png)

