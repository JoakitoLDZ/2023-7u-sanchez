    // Función que muestra un mensaje de bienvenida
    function saludar() {
        alert("¡Bienvenido a la Guía de World of Warcraft! Disfruta explorando el mundo de Azeroth.");
    }

    // Agregar un evento de clic al botón para llamar a la función
    document.getElementById("saludar-btn").addEventListener("click", saludar);